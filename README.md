In this repository I've added a Maven project called Vending Machine. This machine is designed to deliver RATP tickets for transportation.
In order to run the application the user must follow next steps:

1.Choose a ticket from the menu well be showed which contains ticket id, name and price.![menu.PNG](https://bitbucket.org/repo/XoxR6d/images/3824298959-menu.PNG)

2.Enter the desired quantity:                                                                     ![Quantity.PNG](https://bitbucket.org/repo/XoxR6d/images/475753075-Quantity.PNG)

3.Now the user will need to pay for his tickets (e.g: 0.1ron , 0.5ron , 1ron , 5ron , 10ron , 50ron)
![pay.PNG](https://bitbucket.org/repo/XoxR6d/images/1347938681-pay.PNG)

4.After payment and received change, the user will enjoy his product.