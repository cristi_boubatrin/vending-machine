package ro.penatastagiu.vendingmachine_test;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import ro.pentastagiu.impl.TicketStock;
import ro.pentastagiu.models.Ticket;

public class TicketStockTest {
	TicketStock ticketStock = new TicketStock();
	Ticket ticket = new Ticket(1, 10, 2.0, "one trips");

	@Test
	public void test_verifyTicketId() {
		TicketStock stock = new TicketStock();
		stock.addTicketToList(ticket);
		assertEquals(1, stock.getTicketId(ticket));
	}

	@Test
	public void test_verifyTicketQuantity() {
		TicketStock stock = new TicketStock();
		stock.addTicketToList(ticket);
		assertEquals(10, stock.getTicketQuantity(ticket));
	}

	@Test
	public void test_verifyTicketType() {
		TicketStock stock = new TicketStock();
		stock.addTicketToList(ticket);
		assertEquals("one trips", stock.getTicketType(ticket));
	}

	@Test
	public void test_checkQuantity() throws NumberFormatException, IOException {
		ticketStock.addTicketToList(ticket);
		assertEquals(true, ticketStock.checkQuantity(ticket, 10));
	}

}
