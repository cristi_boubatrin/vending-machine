package ro.pentastagiu.models;

/**
 * @author Cristi This class implements the model of a ticket. It contains
 *         setters and getters method and constructors.
 */
public class Ticket {
	private int id;
	private int quantity;
	private double price;
	private String type;

	/**
	 * Constructor without parameters
	 */
	public Ticket() {
		this.id = 0;
		this.quantity = 0;
		this.price = 0.0;
		this.type = null;
	}

	/**
	 * Constructor with parameters
	 * 
	 * @param id
	 * @param quantity
	 * @param price
	 * @param type
	 */
	public Ticket(int id, int quantity, double price, String type) {
		this.id = id;
		this.quantity = quantity;
		this.price = price;
		this.type = type;
	}

	/**
	 * Get ticket id
	 * 
	 * @return id
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * Set ticket id
	 * 
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Set ticket quantity
	 * 
	 * @param quantity
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/**
	 * Get ticket quantity
	 * 
	 * @return quantity
	 */
	public int getQuantity() {
		return this.quantity;
	}

	/**
	 * Set ticket price
	 * 
	 * @param price
	 */
	public void setPrice(int price) {
		this.price = price;

	}

	/**
	 * Get ticket price
	 * 
	 * @return price
	 */
	public double getPrice() {
		return this.price;
	}

	/**
	 * Set ticket type
	 * 
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;

	}

	/**
	 * Get ticket type
	 * 
	 * @return type
	 */
	public String getType() {
		return type;
	}
}