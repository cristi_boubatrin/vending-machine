package ro.pentastagiu.models;

import java.io.IOException;
import java.sql.SQLException;

import ro.pentastagiu.impl.CoinStock;
import ro.pentastagiu.impl.TicketStock;

/**
 * @author Cristi
 *
 */
public interface VendingMachine {

	/**
	 * Start Vending Machine
	 * @throws IOException 
	 * @throws SQLException 
	 */
	void startVendingMachine() throws SQLException;

	/**
	 * This method read coins input by the user while inserted coins < total
	 * payment. After the payment, the users receives the change if the total
	 * payment was exceeded.
	 */
	void readInsertedCoins(Ticket selectedTicket, int quantity);

	/**
	 * This method calculate the change to be received
	 */
	double getChange(double insertedCoins, Ticket selectedTicket, int quantity);

	/**
	 * Set ticket stock
	 * 
	 * @param ticketStock
	 */
	void setTicketStock(TicketStock ticketStock);

	/**
	 * Set coin stock
	 * 
	 * @param coinStock
	 */
	void setCoinStock(CoinStock coinStock);

}