package ro.pentastagiu.models;

/**
 * @author Cristi This class implements the model of a coin. It contains setters
 *         and getters method and constructors.
 *
 */
public class Coin {
	private String coinName;
	private int quantity;
	private double coinValue;

	/**
	 * Constructor without parameters
	 * 
	 */
	public Coin() {
		this.coinName = null;
		this.quantity = 0;
		this.coinValue = 0.0;
	}

	/**
	 * Constructor with parameters
	 * 
	 * @param quantity
	 * @param coinValue
	 */
	public Coin(String coinName, double coinValue, int quantity) {
		this.coinName = coinName;
		this.quantity = quantity;
		this.coinValue = coinValue;
	}

	/**
	 * Get coin name
	 * 
	 * @return coinName
	 */
	public String getCoinName() {
		return coinName;
	}

	/**
	 * Set coin name
	 * 
	 * @param coinName
	 */
	public void setCoinName(String coinName) {
		this.coinName = coinName;
	}

	/**
	 * Get coin quantity
	 * 
	 * @return quantity
	 */
	public int getQuantity() {
		return this.quantity;
	}

	/**
	 * Set coin quantity
	 * 
	 * @param quantity
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/**
	 * Set coin value
	 * 
	 * @param coinValue
	 */
	public void setValue(double coinValue) {
		this.coinValue = coinValue;
	}

	/**
	 * Get coin value
	 * 
	 * @return coinValue
	 */
	public double getValue() {
		return this.coinValue;
	}
}
