package ro.pentastagiu.runner;

import java.io.IOException;
import java.sql.SQLException;

import ro.pentastagiu.impl.VendingMachineImpl;
import ro.pentastagiu.models.VendingMachine;

/**
 *Main class of the program
 */
public class Main {
	/**
	 * The main method
	 * @throws IOException 
	 * @throws SQLException 
	 */
	public static void main(String[] args) throws SQLException  {
		VendingMachine VM = new VendingMachineImpl();
		//Start Vending Machine
		VM.startVendingMachine();
	}
}
