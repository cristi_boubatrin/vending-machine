package ro.pentastagiu.initializers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import ro.pentastagiu.impl.CoinStock;
import ro.pentastagiu.impl.TicketStock;
import ro.pentastagiu.models.Coin;
import ro.pentastagiu.models.Ticket;
import ro.pentastagiu.models.VendingMachine;

public class VendingMachineInit {
	TicketStock ticketStock = new TicketStock();
	CoinStock coinStock = new CoinStock();

	/**
	 * Initialize the Vending Machine
	 */
	public void initializeVM(VendingMachine vm, String ticketsFilePath, String coinsFilePath) {
		try {
			vm.setTicketStock(readTicketsFromFile(ticketsFilePath));
			vm.setCoinStock(readCoinsFromFile(coinsFilePath));
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Read tickets stock from an external file
	 */
	public TicketStock readTicketsFromFile(String file) throws NumberFormatException, IOException {

		String strLine;
		String[] parts;
		// Open the file
		File inFile = new File(file);
		if (inFile.isFile()) {
			FileInputStream inputFile = new FileInputStream(inFile);
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputFile));
			// Read File Line By Line
			while ((strLine = bufferedReader.readLine()) != null) {
				// split by ,
				parts = strLine.split(",");
				ticketStock.addTicketToList(new Ticket((int) Double.parseDouble(parts[0]),
						(int) Double.parseDouble(parts[1]), Double.parseDouble(parts[2]), parts[3]));
			}
			// Close the input stream
			bufferedReader.close();
		}
		return ticketStock;
	}

	/**
	 * Read coins stock from an external file
	 */
	public CoinStock readCoinsFromFile(String file) throws NumberFormatException, IOException {
		String strLine;
		String[] parts;
		File inFile = new File(file);
		if (inFile.isFile()) {
			FileInputStream inputFile = new FileInputStream(inFile);
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputFile));
			// Read File Line By Line
			while ((strLine = bufferedReader.readLine()) != null) {
				// split by ,
				parts = strLine.split(",");
				coinStock.addCoinToList(
						new Coin(parts[0], Double.parseDouble(parts[1]), (int) Double.parseDouble(parts[2])));
			}
			// Close the input stream
			bufferedReader.close();
		}
		return coinStock;
	}

}
