package ro.pentastagiu.initializers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import ro.pentastagiu.impl.CoinStock;
import ro.pentastagiu.impl.TicketStock;
import ro.pentastagiu.models.Coin;
import ro.pentastagiu.models.Ticket;
import ro.pentastagiu.models.VendingMachine;

public class VendingMachineDB {

	private static final String DB_DRIVER = "com.mysql.jdbc.Driver";
	private static final String DB_CONNECTION = "jdbc:mysql://localhost/stock";
	private static final String DB_USER = "root";
	private static final String DB_PASSWORD = "root";

	/**
	 * Initialize the Vending Machine
	 */
	public void initializeVM(VendingMachine vm) throws SQLException {
		try {
			vm.setTicketStock(selectTicketsFromDB());
			vm.setCoinStock(selectCoinsFromDB());
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] argv) {
		try {
			selectTicketsFromDB();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	public static TicketStock selectTicketsFromDB() throws SQLException {

		Connection dbConnection = null;
		Statement statement = null;
		TicketStock ticketStock = new TicketStock();
		String selectTableSQL = "SELECT * FROM ratpStock";
		try {
			dbConnection = getDBConnection();
			statement = dbConnection.createStatement();
			System.out.println(selectTableSQL);
			// execute select SQL stetement
			ResultSet rs = statement.executeQuery(selectTableSQL);
			while (rs.next()) {
				int id = rs.getInt("id");
				int quantity = rs.getInt("quantity");
				double price = rs.getDouble("price");
				String type = rs.getString("type");
				ticketStock.addTicketToList(new Ticket(id, quantity, price, type));
			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {

			if (statement != null) {
				statement.close();
			}
			if (dbConnection != null) {
				dbConnection.close();
			}
		}
		return ticketStock;
	}

	public static CoinStock selectCoinsFromDB() throws SQLException {
		Connection dbConnection = null;
		Statement statement = null;
		CoinStock coinStock = new CoinStock();
		String selectTableSQL = "SELECT * FROM coinStock order by value";
		try {
			dbConnection = getDBConnection();
			statement = dbConnection.createStatement();
			System.out.println(selectTableSQL);
			// execute select SQL stetement
			ResultSet rs = statement.executeQuery(selectTableSQL);
			while (rs.next()) {
				int quantity = rs.getInt("quantity");
				double value = rs.getDouble("value");
				String type = rs.getString("name");
				coinStock.addCoinToList(new Coin(type, value, quantity));
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (dbConnection != null) {
				dbConnection.close();
			}
		}
		return coinStock;
	}

	private static Connection getDBConnection() {

		Connection dbConnection = null;

		try {
			Class.forName(DB_DRIVER);
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		}
		try {
			dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
			return dbConnection;
		} catch (SQLException e) {

			System.out.println(e.getMessage());
		}
		return dbConnection;
	}
}