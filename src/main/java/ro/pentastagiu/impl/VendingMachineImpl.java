package ro.pentastagiu.impl;

import java.io.IOException;
import java.sql.SQLException;

import ro.pentastagiu.initializers.VendingMachineDB;
import ro.pentastagiu.models.Coin;
import ro.pentastagiu.models.Ticket;
import ro.pentastagiu.models.VendingMachine;

public class VendingMachineImpl implements VendingMachine {
	// Make new objects
	private TicketStock ticketStock = new TicketStock();
	private CoinStock coinStock = new CoinStock();
	private DisplayPanel panel = new DisplayPanel();
	private VendingMachineDB vmDB = new VendingMachineDB();

	/**
	 * {@inheritDoc}
	 */
	public void setTicketStock(TicketStock ticketStock) {
		this.ticketStock = ticketStock;

	}

	/**
	 * {@inheritDoc}
	 */
	public void setCoinStock(CoinStock coinStock) {
		this.coinStock = coinStock;
	}

	/**
	 * Start Vending Machine
	 * 
	 * @throws IOException
	 * @throws SQLException
	 */

	public void startVendingMachine() throws SQLException {

		vmDB.initializeVM(this);
		panel.displayMenu(ticketStock);
		int userSelection = panel.readUserSelection();
		Ticket selectedTicket = ticketStock.getTicketBySelection(userSelection);
		panel.displaySelectedTicket(selectedTicket);
		int insertedQuantity = panel.readQuantity();
		// check if quantity inserted is available
		if (ticketStock.checkQuantity(selectedTicket, insertedQuantity) == true) {

			panel.displayConsole(DisplayPanel.PAY + insertedQuantity * selectedTicket.getPrice() + DisplayPanel.RON);
			// panel.displayCoinMenu(coinStock);

			readInsertedCoins(selectedTicket, insertedQuantity);
		} else {
			panel.displayConsole(DisplayPanel.BIGGER_QUANTITY);
		}
		panel.displayConsole(DisplayPanel.GOODBYE_MESSAGE);

	}

	/**
	 * This method read coins input by the user while inserted coins < total
	 * payment. After the payment, the users receives the change if the total
	 * payment was exceeded.
	 */
	public void readInsertedCoins(Ticket selectedTicket, int quantity) {
		double insertedCoins = 0;
		double total = selectedTicket.getPrice() * quantity;
		double rest = total - insertedCoins;
		while (insertedCoins < total) {
			panel.displayConsole(DisplayPanel.COIN);
			panel.displayConsole(DisplayPanel.YOU_INSERTED + insertedCoins + DisplayPanel.RON
					+ DisplayPanel.PLEASE_INSERT + rest + DisplayPanel.RON);
			String insertedName = panel.readCoinName();
			Coin selectedCoin = coinStock.getCoinBySelection(insertedName);
			double selectedCoinValue = coinStock.getCoinValue(selectedCoin);
			insertedCoins = insertedCoins + selectedCoinValue;
			rest = total - insertedCoins;
		}
		getChange(insertedCoins, selectedTicket, quantity);

	}

	/**
	 * This method calculate the change to be received
	 */
	public double getChange(double insertedCoins, Ticket selectedTicket, int quantity) {
		double change = insertedCoins - (selectedTicket.getPrice() * quantity);
		if (change != 0)
			panel.displayConsole(DisplayPanel.CHANGE + change + DisplayPanel.RON);
		return change;

	}

}