package ro.pentastagiu.impl;

import java.util.Map;
import java.util.Scanner;

import ro.pentastagiu.models.Ticket;

/**
 * This class represents user interaction
 *
 */
public class DisplayPanel {
	private Scanner reader;

	public DisplayPanel() {
		reader = new Scanner(System.in);
	}

	/**
	 * Read option chosen by the user
	 * 
	 * @return selection
	 */
	public int readUserSelection() {
		int selection;
		selection = reader.nextInt();
		if (selection == 0) {
			System.out.println("Goodbye!");
			System.exit(0);
		}
		return selection;
	}

	/**
	 * Read quantity chosen by the user
	 * 
	 * @return insertedQuantity
	 */
	public int readQuantity() {
		int insertedQuantity;
		System.out.println("Please insert quantity: ");
		insertedQuantity = reader.nextInt();
		return insertedQuantity;
	}

	/**
	 * Read coin currency chosen by the user
	 * 
	 * @return insertedCurrency
	 */
	public String readCoinName() {
		String insertedCurrency = null;
		insertedCurrency = reader.next();
		return insertedCurrency;
	}

	/**
	 * Display menu with all the ticket
	 * 
	 * @param ticketStock
	 */
	public void displayMenu(TicketStock ticketStock) {
		System.out.println("\t\t TYPE" + "\t\t\tPRICE");
		for (Map.Entry<Integer, Ticket> entry : ticketStock.getTicketMap().entrySet()) {
			Integer key = entry.getKey();
			System.out.println(key + "\t" + entry.getValue().getType() + "\t\t" + entry.getValue().getPrice());
		}
		System.out.println("0. \texit\n\n" + "What type of ticket do you want:");
	}

	/**
	 * Display price and type of ticket chosen by the user
	 */
	public void displaySelectedTicket(Ticket selectedTicket) {
		System.out
				.println("You selected: " + selectedTicket.getType() + " - price " + selectedTicket.getPrice() + "ron");
	}

	public void stopReader() {
		reader.close();
	}

	public void displayConsole(String message) {
		System.out.println(message);
	}

	public static final String PAY = "You have to pay: ";
	public static final String BIGGER_QUANTITY = "You have inserted a quantity bigger than our stock!";
	public static final String YOU_INSERTED = "You inserted: ";
	public static final String PLEASE_INSERT = ". Please insert: ";
	public static final String CHANGE = "Your change is: ";
	public static final String GOODBYE_MESSAGE = "Please take your ticket. Have a nice trip!";
	public static final String RON = "ron";
	public static final String COIN = "Please put in your coin(Eg:0.1ron, 0.5ron, 1ron, 5ron, 10ron, 50ron): ";

}