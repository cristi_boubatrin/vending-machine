package ro.pentastagiu.impl;

import java.util.HashMap;
import java.util.Set;

import ro.pentastagiu.models.Coin;

public class CoinStock {

	private HashMap<String, Coin> coinMap;

	/**
	 * Constructor without parameters
	 */
	public CoinStock() {
		coinMap = new HashMap<String, Coin>();
	}

	/**
	 * Constructor with parameters
	 * 
	 * @param coin
	 */
	public CoinStock(HashMap<String, Coin> coinMap) {
		coinMap = new HashMap<String, Coin>();
		this.coinMap = coinMap;
	}

	public HashMap<String, Coin> getCoinMap() {
		return coinMap;
	}

	public void addCoinToList(Coin coinChoice) {
		coinMap.put(coinChoice.getCoinName(), coinChoice);
	}

	/**
	 * This function return name of thecoinChoise object
	 * 
	 * @param coinChoice
	 * @return coin name
	 */
	public String getCoinName(Coin coinChoice) {
		Set<String> stock = coinMap.keySet();
		for (String currentCoin : stock) {
			if (currentCoin.equals(coinChoice.getCoinName())) {
				return coinChoice.getCoinName();
			}
		}
		return null;
	}

	/**
	 * This function return quantity of the coinChoise object
	 * 
	 * @param coinChoice
	 * @return coin quantity
	 */
	public int getCoinQuantity(Coin coinChoice) {
		Set<String> stock = coinMap.keySet();
		for (String currentCoin : stock) {
			if (currentCoin.equals(coinChoice.getCoinName())) {
				return coinChoice.getQuantity();
			}
		}
		return 0;
	}

	/**
	 * This function return value of the coinChoise object
	 * 
	 * @param coinChoice
	 * @return coin value
	 */
	public double getCoinValue(Coin coinChoice) {
		Set<String> stock = coinMap.keySet();
		for (String currentCoin : stock) {
			if (currentCoin.equals(coinChoice.getCoinName())) {
				return coinChoice.getValue();
			}
		}
		return 0;
	}

	/**
	 * This function has as parameter the user selection. This value is compared
	 * to coin's currency. This object has the same currency as user choose
	 * 
	 * @param selection
	 * @return
	 */
	public Coin getCoinBySelection(String selection) {
		Set<String> stock = coinMap.keySet();
		for (String currentCoin : stock) {
			if (currentCoin.equals(selection)) {
				return coinMap.get(currentCoin);
			}
		}
		return null;
	}

}
