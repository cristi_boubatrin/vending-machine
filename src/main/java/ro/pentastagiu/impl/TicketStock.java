package ro.pentastagiu.impl;

import java.util.HashMap;
import java.util.Set;

import ro.pentastagiu.models.Ticket;

/**
 * @author Cristi
 *
 */
public class TicketStock {

	private HashMap<Integer, Ticket> ticketMap;

	/**
	 * Constructor without parameters
	 * 
	 */
	public TicketStock() {

		ticketMap = new HashMap<Integer, Ticket>();
	}

	/**
	 * Constructor with parameters
	 * 
	 * @param ticket
	 */
	public TicketStock(HashMap<Integer, Ticket> ticketMap) {
		ticketMap = new HashMap<Integer, Ticket>();
		this.ticketMap = ticketMap;
	}

	public HashMap<Integer, Ticket> getTicketMap() {
		return ticketMap;
	}

	/**
	 * Add ticket to ticketStock
	 * 
	 * @param ticketChoice
	 */
	public void addTicketToList(Ticket ticketChoice) {
		ticketMap.put(ticketChoice.getId(), ticketChoice);
	}

	/**
	 * This function return the id of ticketChoice object
	 * 
	 * @param ticketChoice
	 * @return ticket id
	 */
	public int getTicketId(Ticket ticketChoice) {
		Set<Integer> stock = ticketMap.keySet();
		for (Integer currentTicket : stock) {
			if (currentTicket.equals(ticketChoice.getId())) {
				return ticketChoice.getId();
			}
		}
		return 0;
	}

	/**
	 * This function return quantity of the ticketChoice object
	 * 
	 * @param ticketChoice
	 * @return ticket quantity
	 */
	public int getTicketQuantity(Ticket ticketChoice) {
		Set<Integer> stock = ticketMap.keySet();
		for (Integer currentTicket : stock) {
			if (currentTicket.equals(ticketChoice.getId())) {
				return ticketChoice.getQuantity();
			}
		}
		return 0;
	}

	/**
	 * This function return the price of ticketChoise object
	 * 
	 * @param ticketChoise
	 *            Ticket object
	 * @return ticket price
	 */
	public double getTicketPrice(Ticket ticketChoice) {
		Set<Integer> stock = ticketMap.keySet();
		for (Integer currentTicket : stock) {
			if (currentTicket.equals(ticketChoice.getId())) {
				return ticketChoice.getPrice();
			}
		}
		return 0;
	}

	/**
	 * This function return type of the ticketChoice object
	 * 
	 * @param ticketChoice
	 * @return ticket type
	 */
	public String getTicketType(Ticket ticketChoice) {
		Set<Integer> stock = ticketMap.keySet();
		for (Integer currentTicket : stock) {
			if (currentTicket.equals(ticketChoice.getId())) {
				return ticketChoice.getType();
			}
		}
		return null;
	}

	/**
	 * This function has as parameter the user selection. This value is compared
	 * to ticket's id and after that a ticket object is returned. This object
	 * has the same id as user choose
	 * 
	 * @param selection
	 * @return
	 */
	public Ticket getTicketBySelection(int selection) {
		Set<Integer> stock = ticketMap.keySet();
		for (Integer currentTicket : stock) {

			if (currentTicket.equals(selection)) {
				return ticketMap.get(currentTicket);
			}
		}
		return null;
	}

	/**
	 * Check if ticket stock is available. Return true if the stock is available
	 * else, if is out of stock return false
	 * 
	 * @param ticket
	 * @param quantity
	 * @return return True or False
	 */
	public boolean checkQuantity(Ticket ticket, int quantity) {
		if (quantity <= ticket.getQuantity())
			return true;
		return false;
	}
}