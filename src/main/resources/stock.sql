
create database if not exists stock;
use stock;

drop table if exists ratpStock;
create table ratpStock(
id int primary key,
quantity int,
price double,
type varchar(30));

insert into ratpStock values(1,10,2,"Ticket with one trip");
insert into ratpStock values(2,10,4,"Ticket with two trips");

drop table if exists coinStock;
create table coinStock(
name varchar(30) primary key,
value double,
quantity int);

insert into coinStock values("0.1ron",0.1,100);
insert into coinStock values("0.5ron",0.5,100);
insert into coinStock values("1ron",1,100);
insert into coinStock values("5ron",5,100);
insert into coinStock values("10ron",10,100);
insert into coinStock values("50ron",50,100);

